import assert from 'assert'
import { error } from 'console';
class RegisterForm{
  elements = {
    titleInput: () => cy.get('#title'),
    titleFeedback: () => cy.get('#titleFeedback'),
    imageUrlInput: () => cy.get('#imageUrl'),
    urlFeedback: () => cy.get('#urlFeedback'),
    subimitBtn: () => cy.get('#btnSubmit')
  }

  typeTitle(text) {
    if(!text) return;
    this.elements.titleInput().type(text)
  }
  
  typeUrl(text) {
    if(!text) return;
    this.elements.imageUrlInput().type(text)
  }

  clickSubimit() {
       this.elements.subimitBtn().click()
  }
}
const registerForm = new RegisterForm()
const colors = {
  errors: 'rgb(220, 53, 69)',
  success: ''
}
describe('Image Registration', () => {
  describe('Submitting an image with invalid inputs', () => {
    after (() => {
      cy.clearAllLocalStorage()
    })
    const input = {
      title: '',
      url:''
    }
    it('I am on the image registration page', () => {
      cy.visit('/')
  })
    it('I enter "${input.title}" in the title field', () => {
      registerForm.typeTitle('')
    })   
    it('I enter "${input.url}" in the URL field', () => {
      registerForm.typeUrl('')
    })
    it('I click the submit button', () =>{
      registerForm.clickSubimit()
    })
    it('I should see "Please type a title for the image" message above the title field', () => {
      registerForm.elements.titleFeedback().should('contains.text','Please type a title for the image')
    })
    it('I should see "Please type a valid URL" message above the imageUrl field', ()=> {
      registerForm.elements.urlFeedback().should('contains.text','Please type a valid URL')
    })
    it('I should see an exclamation icon in the title and URL fields',() => {
      registerForm.elements.titleInput().should(([element]) => {
        const styles = window.getComputedStyle(element)
        const border = styles.getPropertyValue('border-right-color')
        assert.strictEqual(border, colors.errors)
      })
    })
  })
})

  