import { defineConfig } from "cypress";

export default defineConfig({
  projectId: '7qjsjq',
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl:"https://erickwendel.github.io/vanilla-js-web-app-example/",
    //não vai limpar o estado da tela após cada it
    testIsolation:false
  },
});
